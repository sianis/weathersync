package org.sianis.weathersync;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataItemBuffer;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;


public class MainActivity extends ActionBarActivity {

    private static final String TAG = "MainActivity";
    private static final String WEATHER_API_URL = "http://api.openweathermap.org/data/2.5/weather?q=Budapest,HU&units=metric";
    private static final String WEATHER_MAIN_KEY = "WeatherMain";
    private static final String WEATHER_DESCRIPTION_KEY = "WeatherDescription";
    private static final String WEATHER_TEMP_KEY = "WeatherTemp";
    private static final String WEATHER_ICON_KEY = "WeatherIcon";
    private GoogleApiClient mGoogleApiClient;
    AsyncHttpClient client = new AsyncHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Connect the GoogleApiClient
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();

        findViewById(R.id.deleteButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDataItems();
            }
        });

        findViewById(R.id.sendButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadAndSendCurrentWeather();
            }
        });
    }

    private void deleteDataItems() {
        if (mGoogleApiClient.isConnected()) {
            Wearable.DataApi.getDataItems(mGoogleApiClient).setResultCallback(new ResultCallback<DataItemBuffer>() {
                @Override
                public void onResult(DataItemBuffer dataItems) {
                    for (DataItem dataItem : dataItems) {
                        Wearable.DataApi.deleteDataItems(mGoogleApiClient, dataItem.getUri());
                    }
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Connect to client
        if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Disconnect from client
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void downloadAndSendCurrentWeather() {
        client.get(WEATHER_API_URL, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sendData(response);
                    }
                });
            }
        });
    }

    private void sendData(JSONObject response) {
        if (mGoogleApiClient.isConnected() && response != null) {
            PutDataMapRequest dataMap = PutDataMapRequest.create("/weather");
            JSONObject weather = response.optJSONArray("weather").optJSONObject(0);
            JSONObject main = response.optJSONObject("main");
            dataMap.getDataMap().putString(WEATHER_MAIN_KEY, weather.optString("main"));
            dataMap.getDataMap().putString(WEATHER_DESCRIPTION_KEY, weather.optString("description"));
            dataMap.getDataMap().putString(WEATHER_ICON_KEY, weather.optString("icon"));
            dataMap.getDataMap().putString(WEATHER_TEMP_KEY, main.optString("temp"));
            PutDataRequest request = dataMap.asPutDataRequest();
            PendingResult<DataApi.DataItemResult> pendingResult = Wearable.DataApi.putDataItem(mGoogleApiClient, request);
            pendingResult.setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                @Override
                public void onResult(final DataApi.DataItemResult result) {
                    Status status = result.getStatus();
                    if (status.isSuccess()) {
                        Log.d(TAG, "Data item set: " + result.getDataItem().getUri());
                    } else if (status.isCanceled()) {
                        Log.d(TAG, "Data item canceled");
                    } else if (status.isInterrupted()) {
                        Log.d(TAG, "Data item interrupted");
                    }
                }
            });
        }
    }
}