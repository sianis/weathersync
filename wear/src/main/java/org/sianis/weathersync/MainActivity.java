package org.sianis.weathersync;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItemBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;


public class MainActivity extends Activity implements DataApi.DataListener, GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "MainActivity";
    private static final String WEATHER_MAIN_KEY = "WeatherMain";
    private static final String WEATHER_DESCRIPTION_KEY = "WeatherDescription";
    private static final String WEATHER_TEMP_KEY = "WeatherTemp";
    private static final String WEATHER_ICON_KEY = "WeatherIcon";

    //Communication part
    private GoogleApiClient mGoogleApiClient;

    //Views
    private ProgressBar progress;
    private ImageView icon;
    private TextView temp;
    private TextView main;
    private TextView description;
    private int iconForWeather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progress = (ProgressBar) findViewById(R.id.progress);
        icon = (ImageView) findViewById(R.id.icon);
        temp = (TextView) findViewById(R.id.temp);
        main = (TextView) findViewById(R.id.main);
        description = (TextView) findViewById(R.id.description);

        //Connect the GoogleApiClient
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        } else {
            addListener();
        }
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        if (null != mGoogleApiClient && mGoogleApiClient.isConnected()) {
            //Disconnect data listener
            Wearable.DataApi.removeListener(mGoogleApiClient, this);
            //Disconnect client
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Add data listener
        showDataIfPossible();
        Log.d(TAG, "onConnected");
    }

    private void showDataIfPossible() {
        Wearable.DataApi.getDataItems(mGoogleApiClient).setResultCallback(new ResultCallback<DataItemBuffer>() {
            @Override
            public void onResult(DataItemBuffer dataItems) {
                if (dataItems.getStatus().isSuccess() && dataItems.getCount() > 0) {
                    //Parse only first one
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(dataItems.get(0));
                    if (dataMapItem != null) {
                        showWeatherFromDataMap(dataMapItem.getDataMap());
                    }
                }
                addListener();
            }
        });
    }

    private void addListener() {
        Log.d(TAG, "addListener");
        Wearable.DataApi.addListener(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Not important now
        Log.d(TAG, "onConnectionSuspended");
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.d(TAG, "onDataChanged");
        for (DataEvent dataEvent : dataEvents) {
            switch (dataEvent.getType()) {
                case DataEvent.TYPE_CHANGED:
                    Log.d(TAG, "DataEvent.TYPE_CHANGED");
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(dataEvent.getDataItem());
                    if (dataMapItem != null) {
                        showWeatherFromDataMap(dataMapItem.getDataMap());
                    }
                    break;
                case DataEvent.TYPE_DELETED:
                    Log.d(TAG, "DataEvent.TYPE_DELETED");
                    break;
            }
        }
    }

    private void showWeatherFromDataMap(final DataMap dataMap) {
        if (dataMap.containsKey(WEATHER_MAIN_KEY)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progress.setVisibility(View.GONE);
                    icon.setImageResource(getIconForWeather(dataMap.getString(WEATHER_ICON_KEY)));
                    temp.setText(dataMap.getString(WEATHER_TEMP_KEY) + " °C");
                    main.setText(dataMap.getString(WEATHER_MAIN_KEY));
                    description.setText(dataMap.getString(WEATHER_DESCRIPTION_KEY));
                }
            });
        }
    }

    public int getIconForWeather(String weatherIcon) {
        if (weatherIcon.startsWith("01")) {
            if (weatherIcon.endsWith("d")) {
                return R.drawable.ic_day;
            } else {
                return R.drawable.ic_night;
            }
        } else if (weatherIcon.startsWith("02")) {
            if (weatherIcon.endsWith("d")) {
                return R.drawable.ic_cloudy_day;
            } else {
                return R.drawable.ic_cloudy_night;
            }
        } else if (weatherIcon.startsWith("03")) {
            return R.drawable.ic_scattered_clouds;
        } else if (weatherIcon.startsWith("04")) {
            return R.drawable.ic_broken_clouds;
        } else if (weatherIcon.startsWith("09") || weatherIcon.startsWith("10")) {
            return R.drawable.ic_rain;
        } else if (weatherIcon.startsWith("11")) {
            return R.drawable.ic_storm;
        } else if (weatherIcon.startsWith("13")) {
            return R.drawable.ic_snow;
        } else {
            return R.drawable.ic_fog;
        }
    }
}
